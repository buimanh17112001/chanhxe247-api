import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { UserDTO } from '../dto/user.dto';
import { catchResolverError, generateToken } from '../helpers';
import UserService from '../services/user.service';
import RegisterInput from '../types/user/RegisterInput';
import User from '../types/user/User';
import UserMutationResponse from '../types/user/UserMutationResponse';

@Resolver()
export class UserResolver {
  @Query((_return) => [User])
  async queryUsers(): Promise<User[]> {
    return [];
  }

  @Mutation((_return) => UserMutationResponse)
  async register(
    @Arg('registerInput')
    registerInput: RegisterInput
  ): Promise<UserMutationResponse> {
    try {
      const newUser = await UserService.create(UserDTO.create(registerInput));
      const accessToken = generateToken({
        email: newUser.email,
        userId: newUser._id,
        role: newUser.role,
      });

      return {
        success: true,
        message: 'Đăng ký thành công!',
        code: 'SUCCESS',
        data: newUser,
        accessToken,
      };
    } catch (error) {
      return catchResolverError(error);
    }
  }
}
