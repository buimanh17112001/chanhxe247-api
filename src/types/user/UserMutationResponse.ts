import { Field, ObjectType } from 'type-graphql';
import ResolverResponse from '../graph/ResolverResponse';
import User from './User';

@ObjectType({ implements: ResolverResponse })
export default class UserMutationResponse implements ResolverResponse {
  @Field()
  success: boolean;

  @Field()
  code: string;

  @Field({ nullable: true })
  message?: string;

  @Field({ nullable: true })
  data?: User;

  @Field({ nullable: true })
  accessToken?: string;
}
