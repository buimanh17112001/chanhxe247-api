import { Field, ID, ObjectType } from 'type-graphql';

@ObjectType()
export default class User {
  @Field((_type) => ID)
  _id: string;

  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field()
  email: string;
  
  password: string;
  role: string;
}
