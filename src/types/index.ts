import { Request, Response } from 'express';

export interface Role {}

export interface UserAuthPayload {
  userId: string;
  role: Role;
}
export interface Context {
  req: Request;
  res: Response;
  userAuthPayload: UserAuthPayload;
}