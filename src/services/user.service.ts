import { model, Model } from 'mongoose';
import { hashString } from '../helpers';
import schema from '../schemas/user.schema';
import RegisterInput from '../types/user/RegisterInput';
import User from '../types/user/User';

export default class UserService {
  private static readonly repository: Model<User> = model<User>('user', schema);

  static async create(newData: RegisterInput): Promise<User> {
    const existed = await this.repository.findOne({ email: newData.email });

    if (existed) {
      throw new Error('Email đã tồn tại');
    }

    const hashPassword: string = hashString(newData.password);
    const newUser = await this.repository.create({
      ...newData,
      password: hashPassword,
    });

    return newUser;
  }
}
