import {
  ApolloServerPluginDrainHttpServer,
  ApolloServerPluginLandingPageGraphQLPlayground,
} from 'apollo-server-core';
import { ApolloServer } from 'apollo-server-express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import express, { Application } from 'express';
import { createServer } from 'http';
import 'reflect-metadata';
import { buildSchema } from 'type-graphql';
import database from './configs/database';
import { logger } from './helpers';
import { UserResolver } from './resolvers/user.resolver';
import { Context } from './types';
dotenv.config();

const main = async () => {
  const app: Application = express();

  app.use(cors({ origin: 'http://localhost:3000', credentials: true }));
  app.use(cookieParser());

  const httpServer = createServer(app);
  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      validate: false,
      resolvers: [UserResolver],
    }),
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      ApolloServerPluginLandingPageGraphQLPlayground,
    ],
    context: ({ req, res }): Pick<Context, 'req' | 'res'> => ({ req, res }),
  });

  await apolloServer.start();
  apolloServer.applyMiddleware({
    app,
    cors: { origin: 'http://localhost:3000', credentials: true },
  });

  const port = process.env.PORT || 8080;

  await database.connect();
  httpServer.listen({ port }, () => {
    logger.info(
      `App listened at ${port}, endpoint http://localhost:${port}${apolloServer.graphqlPath}`
    );
  });
};

main();
