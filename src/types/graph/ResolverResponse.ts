import { Field, InterfaceType } from 'type-graphql';

@InterfaceType()
export default abstract class ResolverResponse {
  @Field()
  success: boolean;

  @Field()
  code: string;

  @Field({ nullable: true })
  message?: string;
}
