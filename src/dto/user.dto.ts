import Joi, { ObjectSchema } from 'joi';
import { catchValidationError } from '../helpers';
import { ResponseException } from '../types/ResponseException';
import RegisterInput from '../types/user/RegisterInput';

export class UserDTO {
  private static validate<T>(schema: ObjectSchema<T>, data: T) {
    const { error } = schema.validate(data);
    if (error) {
      throw new ResponseException(catchValidationError(error), 400);
    }

    return data;
  }

  static create(data: any): RegisterInput {
    const { email, firstName, lastName, password } = data;

    const schema = Joi.object({
      role: Joi.string(),
      email: Joi.string()
        .email({ tlds: { allow: false } })
        .required(),
      firstName: Joi.string().min(1),
      lastName: Joi.string().min(1),
      password: Joi.string().min(6),
    });

    return this.validate<RegisterInput>(schema, {
      email,
      firstName,
      lastName,
      password,
    });
  }
}
