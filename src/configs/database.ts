import mongoose from 'mongoose';
import { logger } from '../helpers';

const connect = async (
  retries: number = Number(process.env.DATABASE_CONNECT_RETRIES!)
) => {
  try {
    await mongoose.connect(process.env.DATABASE_URI!);
    logger.info('Connected to db successfully!');
  } catch (error) {
    retries--;

    if (retries > 0) {
      logger.warn('Connected to db failed, reconnecting...');
      connect(retries);
    } else {
      logger.error('Can not connect to db!', error);
    }
  }
};

const disconnect = async (cb?: () => any) => {
  await mongoose.connection.close(true, cb && cb());
};

export default { connect, disconnect };
