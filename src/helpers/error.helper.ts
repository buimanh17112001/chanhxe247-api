import { ValidationError } from 'joi';
import ResolverResponse from '../types/graph/ResolverResponse';

export const catchValidationError = (error: ValidationError) => {
  return error.details.map((err) => err.message).join(' / ');
};

export const catchResolverError = (error: any): ResolverResponse => {
  const message = error?.message || 'Uncaught Error!';
  const statusCode = error.statusCode || 500;

  return {
    code: statusCode,
    message,
    success: false,
  };
};
