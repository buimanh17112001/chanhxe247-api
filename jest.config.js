module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testRunner: 'jest-jasmine2',
  setupFiles: ["dotenv/config"],
  modulePathIgnorePatterns: ["<rootDir>/dist/", "<rootDir>/src/services"],
};
