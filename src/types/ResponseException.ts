export class ResponseException {
  readonly message: string;
  readonly statusCode: number;

  constructor(msg: string, code: number) {
    this.message = msg;
    this.statusCode = code;
  }
}
