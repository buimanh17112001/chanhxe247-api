import cryptoJs from 'crypto-js';
import jwt from 'jsonwebtoken';
import { logger } from '.';
import { Role } from '../types';

export interface JwtTokenPayload {
  email: string;
  userId: string;
  role: string;
}

export const verifyToken = (token: string): JwtTokenPayload | null => {
  try {
    if (!token) return null;

    const verifiedData = jwt.verify(token, process.env.JWT_KEY!);
    if (!verifiedData) return null;

    if (
      typeof verifiedData !== 'string' &&
      verifiedData.exp &&
      Date.now() >= verifiedData.exp * 1000
    )
      return null;

    return verifiedData as JwtTokenPayload;
  } catch (error: any) {
    logger.warn(error.message);
    return null;
  }
};

export const generateToken = (data: JwtTokenPayload) => {
  return jwt.sign(data, process.env.JWT_KEY!, {
    expiresIn: process.env.JWT_EXPIRATION!,
  });
};

export const hashString = (str: string): string => {
  return cryptoJs.AES.encrypt(str, process.env.CRYPTO_KEY!).toString();
};
